package com.hkm.apps.parkour_beta.tObject;

/**
 * Created by jegul on 27/02/2017.
 */

public class tBuilding {

    private String id;
    private String name;
    private String address;
    private String lat;
    private String lng;
    private String floor_count;
    private String hour;

    public tBuilding() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getFloor_count() {
        return floor_count;
    }

    public String getHour() {
        return hour;
    }
}
