package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Adapter.BookingCardAdapter;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.tObject.tBuilding;
import com.hkm.apps.parkour_beta.tObject.tFloor;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentBookingDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar mToolbar;

    @BindView(R.id.booking_building_name) TextView buildingNameTv;
    @BindView(R.id.booking_building_address) TextView buildingAddressTv;
    @BindView(R.id.booking_building_floor) TextView buildingFloorTv;
    @BindView(R.id.booking_building_slot) TextView buildingSlotTv;

    @BindView(R.id.booking_plate_number) TextView plateNumberTv;
    @BindView(R.id.booking_status) TextView statusTv;

    @BindView(R.id.navigate_to_location_button) Button navigateBtn;

    Unbinder unbinder;

    Gson gson = new Gson();

    String latitude;
    String longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_booking_detail);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle("Current Booking Detail");
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Call<ArrayList<tFloor>> callGetBooking = Helper.getParkourService().tCheckBooking(tUser.getInstance().getId());
        callGetBooking.enqueue(new Callback<ArrayList<tFloor>>() {
            @Override
            public void onResponse(Call<ArrayList<tFloor>> call, Response<ArrayList<tFloor>> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(CurrentBookingDetailActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(CurrentBookingDetailActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    tFloor currentFloor = response.body().get(0);
                    buildingFloorTv.setText(currentFloor.getFloor());
                    buildingSlotTv.setText(currentFloor.getSlot_id());

                    String currentPlateNumber = currentFloor.getPlate_number();
                    String modifiedCurrentPlateNumber = Helper.removeZeroFromString(currentPlateNumber);
                    plateNumberTv.setText(modifiedCurrentPlateNumber);

                    if(currentFloor.getStatus().equals("Reserved")){
                        statusTv.setText("Not Arrived");
                    }
                    else if(currentFloor.getStatus().equals("Occupied")){
                        statusTv.setText("Arrived");
                    }

                    getBuildingDetail(currentFloor.getBuilding_id());

//                    navigateBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tFloor>> call, Throwable t) {

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void getBuildingDetail(final String buildingID){
        Call<ArrayList<tBuilding>> callGetBuilding = Helper.getParkourService().tGetBuildingInfo(buildingID);
        callGetBuilding.enqueue(new Callback<ArrayList<tBuilding>>() {
            @Override
            public void onResponse(Call<ArrayList<tBuilding>> call, Response<ArrayList<tBuilding>> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(CurrentBookingDetailActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(CurrentBookingDetailActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    tBuilding currentBuilding = response.body().get(0);
                    buildingNameTv.setText(currentBuilding.getName());
                    buildingAddressTv.setText(currentBuilding.getAddress());

                    latitude = currentBuilding.getLat();
                    longitude = currentBuilding.getLng();

                    navigateBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tBuilding>> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.navigate_to_location_button)
    public void navigateIntent(){
        String uri = "google.navigation:q=".concat(latitude).concat(",").concat(longitude);
        Uri geoLocationURI = Uri.parse(uri);
        Intent intent = new Intent(Intent.ACTION_VIEW, geoLocationURI);
        startActivity(intent);
    }
}
