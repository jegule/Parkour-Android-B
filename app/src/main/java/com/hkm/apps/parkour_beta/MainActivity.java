package com.hkm.apps.parkour_beta;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.tObject.tBuilding;
import com.hkm.apps.parkour_beta.tObject.tFloor;
import com.hkm.apps.parkour_beta.tObject.tPayment;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.fab) FloatingActionButton fabBook;

    Unbinder unbinder;
    GoogleMap gMap;

    String currentPlaceID;

    Gson gson = new Gson();

    Drawer mDrawer;

    private int drawerCounter = 0;

    private DialogInterface.OnClickListener dialogOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, GlobalVar.REQUEST_CODE_FINE_LOCATION_PERMISSION);
        }
    };

    private View.OnClickListener fabOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent detailIntent = new Intent(MainActivity.this, PlaceDetailActivity.class);
//            detailIntent.putExtra("id", currentPlaceID);
            Preferences.getPref().setString("placeID", currentPlaceID);
            startActivityForResult(detailIntent, GlobalVar.REQUEST_CODE_PLACE_DETAIL_ACTIVITY);
        }
    };

    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            gMap = googleMap;
            requestFineLocation();

            /*Call<ParkourResponse<ArrayList<ParkingPlaceResponseObject>>> getPlaceLocationCall = Helper.getParkourService().getPlaceLocation();
            getPlaceLocationCall.enqueue(new Callback<ParkourResponse<ArrayList<ParkingPlaceResponseObject>>>() {
                @Override
                public void onResponse(Call<ParkourResponse<ArrayList<ParkingPlaceResponseObject>>> call, Response<ParkourResponse<ArrayList<ParkingPlaceResponseObject>>> response) {
                    addMarker(response.body().getRespObject());
                }

                @Override
                public void onFailure(Call<ParkourResponse<ArrayList<ParkingPlaceResponseObject>>> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "Cannot get list of place", Toast.LENGTH_SHORT).show();
                }
            });*/

            Call<ArrayList<tBuilding>> callGetBuilding = Helper.getParkourService().tGetBuildings();
            callGetBuilding.enqueue(new Callback<ArrayList<tBuilding>>() {
                @Override
                public void onResponse(Call<ArrayList<tBuilding>> call, Response<ArrayList<tBuilding>> response) {
                    if(response.code() == 200){
                        addMarker(response.body());
                    }
                    else{

                    }
                }

                @Override
                public void onFailure(Call<ArrayList<tBuilding>> call, Throwable t) {

                }
            });
        }
    };

    private GoogleMap.OnMapClickListener onMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            currentPlaceID = null;
            fabBook.hide(true);
        }
    };

    /*private GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            marker.showInfoWindow();
            ParkingPlaceResponseObject temp = gson.fromJson(marker.getTag().toString(), ParkingPlaceResponseObject.class);
            currentPlaceID = temp.getID();
            currentPlaceName = temp.getPlaceName();
            fabBook.show(true);
            return true;
        }
    };*/

    private GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {
            marker.showInfoWindow();
            tBuilding temp = gson.fromJson(marker.getTag().toString(), tBuilding.class);
            currentPlaceID = temp.getId();
            fabBook.show(true);
            return true;
        }
    };

    private Drawer.OnDrawerItemClickListener onDrawerItemClickListener = new Drawer.OnDrawerItemClickListener() {
        @Override
        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
            Intent drawerIntent = null;

            switch(position){
                case 2: {
                    drawerIntent = new Intent(MainActivity.this, CarListActivity.class);
                    startActivityForResult(drawerIntent, GlobalVar.REQUEST_CODE_DRAWER_ITEM);
                    break;
                }
                case 3: {
                    drawerIntent = new Intent(MainActivity.this, CurrentBookingDetailActivity.class);
                    startActivityForResult(drawerIntent, GlobalVar.REQUEST_CODE_DRAWER_ITEM);
                    break;
                }
                case 4: {
                    drawerIntent = new Intent(MainActivity.this, PaymentListActivity.class);
                    startActivityForResult(drawerIntent, GlobalVar.REQUEST_CODE_DRAWER_ITEM);
                    break;
                }
                case 5: {
                    Preferences.getPref().deletePref();
                    drawerIntent = new Intent(MainActivity.this, LoginActivity.class);
                    drawerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(drawerIntent);
                    break;
                }
                case 6: {
                    drawerIntent = new Intent(MainActivity.this, IpActivity.class);
                    startActivity(drawerIntent);
                    break;
                }
                default: {
                    mDrawer.closeDrawer();
                    break;
                }
            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Preferences.init(this);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle(R.string.main_actv);

        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) {
                Picasso.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
            }
        });

        mDrawer = new DrawerBuilder()
                            .withActivity(this)
                            .addDrawerItems(
                                    makeDrawerItem("Search Here", null),
                                    makeDrawerItem("My Cars", null),
                                    makeDrawerItemWithBadge("Current Booking", null),
                                    makeDrawerItemWithBadge("Pending Payment", null),
                                    makeDrawerItem("LogOut", null),
                                    makeDrawerItem("IP", null)
                            )
                            .withOnDrawerItemClickListener(onDrawerItemClickListener)
                            .withToolbar(mToolbar)
                            .withAccountHeader(makeAccountHeader())
                            .build();

        checkBooking();
        checkPendingPayment();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(onMapReadyCallback);

        fabBook.setOnClickListener(fabOnClickListener);

        fabBook.hide(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    //Add Marker based on Array of ParkingPlace
    /*private void addMarker(ArrayList<ParkingPlaceResponseObject> placeList){
        for (int i = 0; i < placeList.size(); i++){
            double latitude = placeList.get(i).getLocation().getLatitude();
            double longitude = placeList.get(i).getLocation().getLongitude();
            LatLng loc = new LatLng(latitude, longitude);

            String title = placeList.get(i).getPlaceName();

            Marker marker = gMap.addMarker(new MarkerOptions().position(loc).title(title));
            String temp = gson.toJson(placeList.get(i));
            marker.setTag(temp);
        }
        gMap.setOnMarkerClickListener(onMarkerClickListener);
        gMap.setOnMapClickListener(onMapClickListener);
    }*/

    private void addMarker(ArrayList<tBuilding> buildingList){
        for (int i = 0; i < buildingList.size(); i++){
            double latitude = Double.parseDouble(buildingList.get(i).getLat());
            double longitude = Double.parseDouble(buildingList.get(i).getLng());
            LatLng loc = new LatLng(latitude, longitude);

            String title = buildingList.get(i).getName();

            Marker marker = gMap.addMarker(new MarkerOptions().position(loc).title(title));
            String temp = gson.toJson(buildingList.get(i));
            marker.setTag(temp);
        }
        gMap.setOnMarkerClickListener(onMarkerClickListener);
        gMap.setOnMapClickListener(onMapClickListener);
    }


    private AccountHeader makeAccountHeader(){
        /*String fullName = UserResponseObject.getInstance().getFullName();
        String email = UserResponseObject.getInstance().getEmail();*/

        String fullName = tUser.getInstance().getUsername();
        Toast.makeText(this, fullName, Toast.LENGTH_SHORT).show();
        String email = tUser.getInstance().getEmail();

        return new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.account_header_color)
                .addProfiles(makeProfile(fullName, email, null))
                .withOnAccountHeaderProfileImageListener(new AccountHeader.OnAccountHeaderProfileImageListener() {
                    @Override
                    public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
//                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        startActivity(intent);
                        return false;
                    }

                    @Override
                    public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
                        return false;
                    }
                })
                .withSelectionListEnabledForSingleProfile(false)
                .build();
    }

    private ProfileDrawerItem makeProfile(String name, String email, @Nullable Drawable icon) {
        return new ProfileDrawerItem()
                    .withName(name)
                    .withEmail(email)
                    .withIcon(tUser.getInstance().getPicture());
    }

    private PrimaryDrawerItem makeDrawerItem(String name, @Nullable Drawable icon){
        return new PrimaryDrawerItem()
                    .withIdentifier(drawerCounter++)
                    .withName(name)
                    .withIcon(icon)
                    .withSelectable(false);
    }

    private PrimaryDrawerItem makeDrawerItemWithBadge(String name, @Nullable Drawable icon){
        return new PrimaryDrawerItem()
                .withIdentifier(drawerCounter++)
                .withBadgeStyle(new BadgeStyle().withTextColor(Color.WHITE).withColorRes(R.color.md_red_700))
                .withName(name)
                .withIcon(icon)
                .withSelectable(false);
    }

    private void requestFineLocation(){
        int hasFineLocationPermission = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(hasFineLocationPermission != PackageManager.PERMISSION_GRANTED){
            if(!ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)){
                Helper.okCancelDialog(this,
                        "You need to give permission for Parkour to access your current location",
                        dialogOnClickListener);
            }
            else{
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, GlobalVar.REQUEST_CODE_FINE_LOCATION_PERMISSION);
            }
        }
        else{
            gMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == GlobalVar.REQUEST_CODE_FINE_LOCATION_PERMISSION){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                enableMyLocation();
            }
            else{
                Toast.makeText(MainActivity.this, "Access Current Location failed", Toast.LENGTH_SHORT).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void enableMyLocation(){
        int hasFineLocationPermission = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(hasFineLocationPermission == PackageManager.PERMISSION_GRANTED){
            gMap.setMyLocationEnabled(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GlobalVar.REQUEST_CODE_PLACE_DETAIL_ACTIVITY){
            /*if(resultCode == GlobalVar.INTENT_FAILED_CODE){

            }
            else if(resultCode == GlobalVar.INTENT_SUCCESS_CODE){
                checkBooking();
            }*/
            checkBooking();
            checkPendingPayment();
        }
        else if(requestCode == GlobalVar.REQUEST_CODE_DRAWER_ITEM){
            checkBooking();
            checkPendingPayment();
        }
    }

    private void checkBooking(){
        Call<ArrayList<tFloor>> callGetCurrentBooking = Helper.getParkourService().tCheckBooking(tUser.getInstance().getId());
        callGetCurrentBooking.enqueue(new Callback<ArrayList<tFloor>>() {
            @Override
            public void onResponse(Call<ArrayList<tFloor>> call, Response<ArrayList<tFloor>> response) {
                if(response.code() == 404){
                    updateBookingDrawerBadge(0);
                    try{
//                        Toast.makeText(MainActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(MainActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    int totalBook = response.body().size();
                    updateBookingDrawerBadge(totalBook);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tFloor>> call, Throwable t) {

            }
        });
    }

    private void updateBookingDrawerBadge(int total){
        if(total == 0){
            mDrawer.updateBadge(2 , null);
        }
        else{
            mDrawer.updateBadge(2, new StringHolder(Integer.toString(total)));
        }
    }

    private void checkPendingPayment(){
        Call<ArrayList<tPayment>> callGetPendingPayment = Helper.getParkourService().tGetPendingPayment(tUser.getInstance().getId());
        callGetPendingPayment.enqueue(new Callback<ArrayList<tPayment>>() {
            @Override
            public void onResponse(Call<ArrayList<tPayment>> call, Response<ArrayList<tPayment>> response) {
                if(response.code() == 404){
                    updatePendingPaymentDrawerBadge(0);
                    try{
//                        Toast.makeText(MainActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(MainActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    int totalPendingPayment = response.body().size();
                    updatePendingPaymentDrawerBadge(totalPendingPayment);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tPayment>> call, Throwable t) {

            }
        });
    }

    private void updatePendingPaymentDrawerBadge(int total){
        if(total == 0){
            mDrawer.updateBadge(3 , null);
        }
        else{
            mDrawer.updateBadge(3, new StringHolder(Integer.toString(total)));
        }
    }

}
