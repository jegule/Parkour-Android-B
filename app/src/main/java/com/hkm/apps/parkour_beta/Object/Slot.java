package com.hkm.apps.parkour_beta.Object;

/**
 * Created by jegul on 16/02/2017.
 */

public class Slot {

    private String id;
    private boolean availability;

    public Slot(){}

    public String getId() {
        return id;
    }

    public boolean isAvailability() {
        return availability;
    }
}
