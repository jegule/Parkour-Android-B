package com.hkm.apps.parkour_beta.tObject;

import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Preferences;

/**
 * Created by jegul on 26/02/2017.
 */

public class tUser {

    private String id;
    private String username;
    private String email;
//    private String password;

    private String ektp;
    private String picture;

    private String join;
//    private String plate_number;


    public tUser() {
    }

    public static tUser getInstance(){
        return Preferences.getPref().getJson(tUser.class);
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getEktp() {
        return ektp;
    }

    public String getPicture() {
        return GlobalVar.BASE_URI + picture;
//        return Preferences.getPref().getString("ip", "http://192.168.0.195") + GlobalVar.BASE_URI_ + picture;
    }

    public String getJoin() {
        return join;
    }
}
