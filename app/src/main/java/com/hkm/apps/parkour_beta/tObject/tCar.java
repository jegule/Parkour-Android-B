package com.hkm.apps.parkour_beta.tObject;

/**
 * Created by jegul on 03/03/2017.
 */

public class tCar {

    private String id;
    private String user_id;
    private String plate_number;
    private String type;
    private String brand;
    private String picture;

    public tCar() {
    }

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPlate_number() {
        return plate_number;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getPicture() {
        return picture;
    }
}
