package com.hkm.apps.parkour_beta.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by jegul on 16/02/2017.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {

    ArrayList<String> mArrayList;

    public SpinnerAdapter(Context context, int resource, ArrayList<String> object) {
        super(context, resource, object);
        mArrayList = object;
    }

    @Override
    public void add(String object) {
        mArrayList.add(object);
        Collections.sort(mArrayList);
//        notifyDataSetChanged();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public void clear() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    public void refresh(ArrayList<String> al){
        this.mArrayList = al;
        notifyDataSetChanged();
    }

    @Override
    public void addAll(Collection<? extends String> collection) {
        mArrayList.clear();
        if(collection != null){
            mArrayList.addAll(collection);
        }
        notifyDataSetChanged();
    }
}
