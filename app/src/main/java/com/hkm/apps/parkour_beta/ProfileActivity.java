package com.hkm.apps.parkour_beta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.hkm.apps.parkour_beta.tObject.tUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.profile_full_name) TextView fNameTv;
    @BindView(R.id.profile_email) TextView emailTv;
    /*@BindView(R.id.profile_ektp_uid) TextView eKtpTv;
    @BindView(R.id.profile_join_date) TextView joinDateTv;*/

    @BindView(R.id.toolbar) Toolbar mToolbar;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle(R.string.profile_actv);
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String fullName = tUser.getInstance().getUsername();
        String email = tUser.getInstance().getEmail();
        String eKtpUID = tUser.getInstance().getEktp();
//        Date joinDate = tUser.getInstance().getJoin();
        String joinDate = tUser.getInstance().getJoin();

        fNameTv.setText(fullName);
        emailTv.setText(email);
//        eKtpTv.setText(eKtpUID);
//        joinDateTv.setText(Helper.getCalendarDateString(joinDate));
//        joinDateTv.setText(joinDate);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
