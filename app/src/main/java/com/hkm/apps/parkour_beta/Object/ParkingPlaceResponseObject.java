package com.hkm.apps.parkour_beta.Object;

/**
 * Created by jegul on 15/02/2017.
 */

public class ParkingPlaceResponseObject {

    private String _id;
    private Location location;
    private String placeName;

    private Time open;
    private Time close;
    private String address;

    private Floor[] floor;

    public ParkingPlaceResponseObject(){

    }

    public String getID(){
        return _id;
    }

    public Location getLocation(){
        return location;
    }

    public String getPlaceName(){
        return placeName;
    }

    public Time getOpenTime() {
        return open;
    }

    public Time getCloseTime() {
        return close;
    }

    public String getAddress() {
        return address;
    }

    public Floor[] getFloor() {
        return floor;
    }

}
