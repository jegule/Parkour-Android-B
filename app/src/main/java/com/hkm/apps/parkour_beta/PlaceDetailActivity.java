package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.Object.ParkingPlaceResponseObject;
import com.hkm.apps.parkour_beta.Object.ParkourResponse;
import com.hkm.apps.parkour_beta.tObject.tBuilding;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceDetailActivity extends AppCompatActivity {

    @BindView(R.id.place_name) TextView placeNameTv;
    @BindView(R.id.place_address) TextView placeAddressTv;
    @BindView(R.id.place_hour_open) TextView placeOpenTimeTv;
    @BindView(R.id.place_hour_close) TextView placeCloseTimeTv;

    @BindView(R.id.slot_chooser_button) Button toSlotChooserBtn;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    Unbinder unbinder;
    String id;
    String floorCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle(R.string.place_detail_actv);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        execute();
    }

    private void execute(){
        /*Intent intent = getIntent();
        id = intent.getStringExtra("id");*/
        id = Preferences.getPref().getString("placeID", null);

        /*Call<ParkourResponse<ParkingPlaceResponseObject>> getPlaceDetailCall = Helper.getParkourService().getPlaceDetail(id);
        getPlaceDetailCall.enqueue(new Callback<ParkourResponse<ParkingPlaceResponseObject>>() {
            @Override
            public void onResponse(Call<ParkourResponse<ParkingPlaceResponseObject>> call, Response<ParkourResponse<ParkingPlaceResponseObject>> response) {
                ParkingPlaceResponseObject placeDetail = response.body().getRespObject();
                placeNameTv.setText(placeDetail.getPlaceName());
                placeAddressTv.setText(placeDetail.getAddress());
                placeOpenTimeTv.setText(placeDetail.getOpenTime().toString());
                placeCloseTimeTv.setText(placeDetail.getCloseTime().toString());
            }

            @Override
            public void onFailure(Call<ParkourResponse<ParkingPlaceResponseObject>> call, Throwable t) {

            }
        });*/

        Call<ArrayList<tBuilding>> callGetBuildingInfo = Helper.getParkourService().tGetBuildingInfo(id);
        callGetBuildingInfo.enqueue(new Callback<ArrayList<tBuilding>>() {
            @Override
            public void onResponse(Call<ArrayList<tBuilding>> call, Response<ArrayList<tBuilding>> response) {
                tBuilding buildingDetail = response.body().get(0);
                placeNameTv.setText(buildingDetail.getName());
                Preferences.getPref().setString("placeName", buildingDetail.getName());
                placeAddressTv.setText(buildingDetail.getAddress());
                placeOpenTimeTv.setText(Helper.getBuildingOpenHour(buildingDetail.getHour()));
                placeCloseTimeTv.setText(Helper.getBuildingCloseHour(buildingDetail.getHour()));

                toSlotChooserBtn.setEnabled(true);

                floorCount = buildingDetail.getFloor_count();
                Preferences.getPref().setString("floorCount", floorCount);
            }

            @Override
            public void onFailure(Call<ArrayList<tBuilding>> call, Throwable t) {

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.slot_chooser_button)
    public void goToSlotChooser(){
        Intent intent = new Intent(this, SlotChooserActivity.class);
        intent.putExtra("id", id);
        startActivityForResult(intent, GlobalVar.REQUEST_CODE_SLOT_CHOOSER_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GlobalVar.REQUEST_CODE_SLOT_CHOOSER_ACTIVITY){
            if(resultCode == GlobalVar.INTENT_FAILED_CODE){
                execute();
            }
            else if(resultCode == GlobalVar.INTENT_SUCCESS_CODE){
                Intent intent = new Intent();
                setResult(GlobalVar.INTENT_SUCCESS_CODE, intent);
                finish();
            }
        }
    }

}
