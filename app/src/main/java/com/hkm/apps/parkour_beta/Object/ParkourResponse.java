package com.hkm.apps.parkour_beta.Object;

/**
 * Created by jegul on 15/02/2017.
 */

public class ParkourResponse<T> {

    protected String respType;
    protected String respMessage;
    protected T respObject;

    public String getRespType(){
        return respType;
    }
    public String getRespMessage(){
        return respMessage;
    }
    public T getRespObject() {return respObject; }

}
