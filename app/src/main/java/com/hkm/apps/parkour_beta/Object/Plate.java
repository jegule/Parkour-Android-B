package com.hkm.apps.parkour_beta.Object;

import java.util.Date;

/**
 * Created by jegul on 19/02/2017.
 */

public class Plate {

    private String plate;
    private Date lastUsed;

    public Plate(){}

    public String getPlate() {
        return plate;
    }

    public Date getLastUsed() {
        return lastUsed;
    }
}
