package com.hkm.apps.parkour_beta.Helper;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.hkm.apps.parkour_beta.MainActivity;
import com.hkm.apps.parkour_beta.RetrofitService.ParkourService;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jegul on 11/02/2017.
 */

public class Helper {

    public static ParkourService getParkourService(){
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Preferences.getPref().getString("ip", "http://192.168.43.238") + GlobalVar.BASE_API_URI_)
//                .baseUrl(GlobalVar.BASE_API_URI)
                .baseUrl(Preferences.getPref().getString("ip", "http://192.168.43.238") + GlobalVar.BASE_API_URI_)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ParkourService.class);
    }

    /*public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }*/

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }

    public static String getCalendarDateString(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        StringBuilder builder = new StringBuilder();
        builder.append(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US))
                .append(", ")
                .append(calendar.get(Calendar.DAY_OF_MONTH))
                .append(" ")
                .append(calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US))
                .append(" ")
                .append(calendar.get(Calendar.YEAR));

        return builder.toString();
    }

    public static String getBuildingOpenHour(String date){
        int indexOfSeparator = date.indexOf("-");
        if(indexOfSeparator != -1){
            return date.substring(0, indexOfSeparator);
        }
        else{
            return null;
        }
    }

    public static String getBuildingCloseHour(String date){
        int indexOfSeparator = date.indexOf("-");
        if(indexOfSeparator != -1){
            return date.substring(indexOfSeparator + 1, date.length());
        }
        else{
            return null;
        }
    }

    public static void okCancelDialog(Context ctx, String message, DialogInterface.OnClickListener onClickListener){
        new AlertDialog.Builder(ctx)
                .setMessage(message)
                .setPositiveButton("Ok", onClickListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public static String removeLowerCase(String sourceString){
        String destString = "";
        for(char c : sourceString.toCharArray()){
            if(!Character.isLowerCase(c)){
                destString += c;
            }
        }

        return destString;
    }

    public static String removeZeroFromString(String sourceString){
        StringBuilder destString = new StringBuilder(sourceString);

        char[] sourceCharArray = sourceString.toCharArray();

        boolean isChar = false;

        for(int i = sourceCharArray.length-1; i >= 0; i--){

            if(sourceCharArray[i] != '0'){
                break;
            }
            else{
                destString.deleteCharAt(i);
            }
        }

        return destString.toString();
    }

}
