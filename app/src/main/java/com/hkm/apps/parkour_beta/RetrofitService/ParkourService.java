package com.hkm.apps.parkour_beta.RetrofitService;

import com.hkm.apps.parkour_beta.Object.ParkingPlaceResponseObject;
import com.hkm.apps.parkour_beta.Object.ParkourResponse;
import com.hkm.apps.parkour_beta.Object.UserResponseObject;
import com.hkm.apps.parkour_beta.tObject.tBuilding;
import com.hkm.apps.parkour_beta.tObject.tCar;
import com.hkm.apps.parkour_beta.tObject.tFloor;
import com.hkm.apps.parkour_beta.tObject.tPayment;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by jegul on 11/02/2017.
 */

public interface ParkourService {

    @FormUrlEncoded
    @POST("user/register")
    Call<ParkourResponse<UserResponseObject>> userRegister(@Field("email") String email,
                                                           @Field("fullName") String fullName,
                                                           @Field("password") String password,
                                                           @Field("eKtpUID") String eKtpUID);

    @FormUrlEncoded
    @POST("user/login")
    Call<ParkourResponse<UserResponseObject>> userLogin(@Field("email") String email,
                                                        @Field("password") String password);


    @GET("parkingPlace/location")
    Call<ParkourResponse<ArrayList<ParkingPlaceResponseObject>>> getPlaceLocation();

    @GET("parkingPlace/{id}")
    Call<ParkourResponse<ParkingPlaceResponseObject>> getPlaceDetail(@Path("id") String id);

    @GET("parkingPlace/{id}/{floor}")
    Call<ParkourResponse<ParkingPlaceResponseObject>> getFloorDetail(@Path("id") String id,
                                                                     @Path("floor") int floor);

    @FormUrlEncoded
    @POST("user/addCar")
    Call<ParkourResponse<UserResponseObject>> addCarPlate(@Header("x_user_id") String userID,
                                                          @Field("plateNumber") String plateNumber);

    //db_hans

    @FormUrlEncoded
    @POST("register")
    Call<tTemplate> tUserRegister(@Field("username") String fullName,
                                  @Field("email") String email,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("login")
    Call<ArrayList<tUser>> tUserLogin(@Field("email") String email,
                                      @Field("password") String password);

    @GET("allbuilding")
    Call<ArrayList<tBuilding>> tGetBuildings();

    @GET("searchbuilding/id/{building_id}")
    Call<ArrayList<tBuilding>> tGetBuildingInfo(@Path("building_id") String building_id);

    @GET("searchfloor/building/{building_id}/floor/{floor_id}")
    Call<ArrayList<tFloor>> tGetFloor(@Path("building_id") String building_id,
                                      @Path("floor_id") String floor_id);

    @FormUrlEncoded
    @POST("myCar")
    Call<ArrayList<tCar>> tGetCar(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("registerCar")
    Call<Boolean> tAddCar(@Field("user_id") String userID,
                          @Field("plate_number") String plateNumber);

    @FormUrlEncoded
    @POST("reserve")
    Call<tTemplate> tBookSlot(@Field("user_id") String userID,
                              @Field("plate_number") String plateNumber,
                              @Field("slot_id") String slotID,
                              @Field("building_id") String buildingID);

    @FormUrlEncoded
    @POST("reservationCheck")
    Call<ArrayList<tFloor>> tCheckBooking(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("insertPayment")
    Call<Boolean> tStartTime(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("showUnpaid")
    Call<ArrayList<tPayment>> tGetPendingPayment(@Field("user_id") String userID);

    @FormUrlEncoded
    @POST("updatePayment")
    Call<Boolean> tPaySingle(@Field("user_id") String userID,
                             @Field("id") String paymentID);

    @FormUrlEncoded
    @POST("updatePaymentAll")
    Call<Boolean> tPayAll(@Field("user_id") String userID);

}
