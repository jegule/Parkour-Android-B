package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingSummaryActivity extends AppCompatActivity {

    @BindView(R.id.summary_place_name) TextView placeNameTv;
    @BindView(R.id.summary_floor) TextView floorTv;
    @BindView(R.id.summary_slot) TextView slotTv;
    @BindView(R.id.summary_car_used) TextView carTv;

    @BindView(R.id.book_now_btn) Button bookBtn;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    Unbinder unbinder;

    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_summary);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle("Booking Summary");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        placeNameTv.setText(Preferences.getPref().getString("placeName", null));
        floorTv.setText(Preferences.getPref().getString("floorID", null));
        slotTv.setText(Preferences.getPref().getString("slotID", null));
        carTv.setText(Preferences.getPref().getString("carID", null));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.book_now_btn)
    public void bookNow(){
        String userID = tUser.getInstance().getId();
        String slotID = Preferences.getPref().getString("slotID", null);
        String plateNumber = Preferences.getPref().getString("carID", null);
        String buildingID = Preferences.getPref().getString("placeID", null);

        while(plateNumber.length() < 10){
            plateNumber += "0";
        }

        Call<tTemplate> callBookSlot = Helper.getParkourService().tBookSlot(userID, plateNumber, slotID, buildingID);
        callBookSlot.enqueue(new Callback<tTemplate>() {
            @Override
            public void onResponse(Call<tTemplate> call, Response<tTemplate> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(BookingSummaryActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(BookingSummaryActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if(response.body().getStatus()){
                        //startTime(response.body().getMessage());
                        Intent intent = new Intent();
                        setResult(GlobalVar.INTENT_SUCCESS_CODE, intent);
                        finish();
                        Toast.makeText(BookingSummaryActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<tTemplate> call, Throwable t) {

            }
        });
    }

    /*@Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(GlobalVar.INTENT_FAILED_CODE, intent);
        finish();
    }*/

    /*private void startTime(final String message){
        Call<Boolean> callStartTime = Helper.getParkourService().tStartTime(tUser.getInstance().getId());
        callStartTime.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(BookingSummaryActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(BookingSummaryActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if(response.body().equals(true)){
                        Intent intent = new Intent();
                        setResult(GlobalVar.INTENT_SUCCESS_CODE, intent);
                        finish();
                        Toast.makeText(BookingSummaryActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(BookingSummaryActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }*/

}
