package com.hkm.apps.parkour_beta.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.hkm.apps.parkour_beta.CarChooserActivity;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.R;
import com.hkm.apps.parkour_beta.tObject.tFloor;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by jegul on 17/07/2017.
 */

public class ButtonGridAdapter extends ArrayAdapter<tFloor> {

    ArrayList<tFloor> slotList;
    int currentFloorNumber = 0;

    public ButtonGridAdapter(@NonNull Context context, ArrayList<tFloor> slotList) {
        super(context, 0, slotList);
        this.slotList = slotList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getView(position, convertView, parent);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_view_btn, parent, false);
        }

        tFloor currentFloor = slotList.get(position);

        Button btn = (Button) convertView.findViewById(R.id.slot_btn);
        btn.setText(currentFloor.getSlot_id());

        switch (currentFloor.getStatus()){
            case "Reserved": {
                btn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.md_blue_A400));
                btn.setEnabled(false);
                break;
            }
            case "Available": {
                btn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.md_green_A700));
                btn.setEnabled(true);
                break;
            }
            case "Occupied": {
                btn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.md_red_A200));
                btn.setEnabled(false);
                break;
            }
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CarChooserActivity.class);
                intent.putExtra("floorID", currentFloorNumber);
                intent.putExtra("slotID", ((Button)v).getText().toString());
                Preferences.getPref().setString("floorID", Integer.toString(currentFloorNumber));
                Preferences.getPref().setString("slotID", ((Button)v).getText().toString());
                ((AppCompatActivity)getContext()).startActivityForResult(intent, GlobalVar.REQUEST_CODE_CAR_CHOOSER_ACTIVITY);
            }
        });

        return convertView;
    }

    public void setCurrentFloorNumber(int floorNum){
        currentFloorNumber = floorNum;
    }

    public void addAll(ArrayList<tFloor> floorList) {

        slotList.clear();
        slotList.addAll(floorList);
        notifyDataSetChanged();
    }
}
