package com.hkm.apps.parkour_beta.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hkm.apps.parkour_beta.R;
import com.hkm.apps.parkour_beta.tObject.tFloor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static android.R.attr.resource;

/**
 * Created by jegul on 08/03/2017.
 */

public class BookingCardAdapter extends ArrayAdapter<tFloor> {

    private ArrayList<tFloor> mFloorList;

    public BookingCardAdapter(@NonNull Context context, ArrayList<tFloor> bookingList) {
        super(context, 0, bookingList);
        mFloorList = bookingList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.booking_card_view, parent, false);
        }

        tFloor currentBooking = mFloorList.get(position);

        TextView titleTv = (TextView) convertView.findViewById(R.id.booking_card_title);
        titleTv.setText(currentBooking.getFloor());

        TextView subtitleTv = (TextView) convertView.findViewById(R.id.booking_card_subtitle);
        subtitleTv.setText(currentBooking.getPlate_number());

        return convertView;
    }

    @Override
    public void addAll(@NonNull Collection<? extends tFloor> collection) {
//        super.addAll(collection);
        mFloorList.clear();
        mFloorList.addAll(collection);
        notifyDataSetChanged();
    }
}
