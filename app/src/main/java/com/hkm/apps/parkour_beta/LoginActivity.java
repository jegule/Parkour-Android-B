package com.hkm.apps.parkour_beta;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.hkm.apps.parkour_beta.Adapter.LoginPagerAdapter;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.tObject.tUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.login_view_pager)
    ViewPager loginViewPager;
    @BindView(R.id.login_tab_layout)
    TabLayout loginTabLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Preferences.init(this);

        if(tUser.getInstance() != null){
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        unbinder = ButterKnife.bind(this);

        LoginPagerAdapter loginPagerAdapter = new LoginPagerAdapter(getSupportFragmentManager(), this);

        loginViewPager.setAdapter(loginPagerAdapter);

        loginTabLayout.setupWithViewPager(loginViewPager);

        toolbar.setTitle(R.string.login_reg_actv);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
