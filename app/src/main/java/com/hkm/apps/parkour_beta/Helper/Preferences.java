package com.hkm.apps.parkour_beta.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

/**
 * Created by jegul on 11/02/2017.
 */

public class Preferences {
    private static Preferences mPref;
    private static SharedPreferences mSharedPref;
    private static Gson gson = new Gson();

    public static void init(Context ctx){
        if(mPref == null) mPref = new Preferences();
        mSharedPref = ctx.getSharedPreferences("parkour", Context.MODE_PRIVATE);
    }

    private Preferences(){}

    public static Preferences getPref(){
        return mPref;
    }

    public void setString(String key, String value){
        mSharedPref.edit().putString(key, value).apply();
    }

    public String getString(String key, String defValue){
        return mSharedPref.getString(key, defValue);
    }

    public <T> T getJson(Class<T> tClass){
        String json = getString(tClass.getSimpleName(), null);
        if(!TextUtils.isEmpty(json))
            return gson.fromJson(json, tClass);
        else
            return null;
    }

    public static void deletePref(){
        mSharedPref.edit().clear().commit();
    }
}
