package com.hkm.apps.parkour_beta.Object;

/**
 * Created by jegul on 16/02/2017.
 */

public class Floor {

    private int number;
    private Slot[] slot;
    private String picture;

    public Floor(){}

    public int getNumber() {
        return number;
    }

    public Slot[] getSlot() {
        return slot;
    }

    public String getPicture() {
        return picture;
    }
}
