package com.hkm.apps.parkour_beta.Object;

import com.hkm.apps.parkour_beta.Helper.Preferences;

import java.util.Date;

/**
 * Created by jegul on 11/02/2017.
 */

public class UserResponseObject {

    private String _id;

    private String fullName;
    private String email;
    private Date join;
    private String eKtpUID;

    private Plate[] plateList;

    public static UserResponseObject getInstance(){
        return Preferences.getPref().getJson(UserResponseObject.class);
    }

    public String getEKtpUID(){
        return eKtpUID;
    }

    public String getFullName(){
        return fullName;
    }

    public String getEmail(){
        return email;
    }

    public Date getJoinDate(){
        return join;
    }

    public Plate[] getPlateList() {
        return plateList;
    }

    public String getID() {
        return _id;
    }
}