package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Adapter.PaymentListAdapter;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.tObject.tPayment;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentListActivity extends AppCompatActivity {

    @BindView(R.id.payment_list_view) ListView paymentListView;
    @BindView(R.id.pay_all_button) Button payAllBtn;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    PaymentListAdapter paymentListAdapter;
    ArrayList<tPayment> paymentList;

    Unbinder unbinder;

    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle("Pending Payment List");
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        paymentList = new ArrayList<>();
        paymentListAdapter = new PaymentListAdapter(this, paymentList);
        paymentListView.setAdapter(paymentListAdapter);

        getPaymentList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        payAllBtn.setEnabled(false);
        if(requestCode == GlobalVar.REQUEST_CODE_STRIPE){
            if(resultCode == RESULT_OK){
                if(!data.getBooleanExtra("allFlag", false)){
                    String paymentID = data.getStringExtra("paymentID");
                    updatePaymentStatus(paymentID);
                }
                else{
                    updateAllPaymentStatus();
                }
            }
            else{
                payAllBtn.setEnabled(true);
            }
        }

    }

    private void getPaymentList(){
        Call<ArrayList<tPayment>> callGetPaymentList = Helper.getParkourService().tGetPendingPayment(tUser.getInstance().getId());
        callGetPaymentList.enqueue(new Callback<ArrayList<tPayment>>() {
            @Override
            public void onResponse(Call<ArrayList<tPayment>> call, Response<ArrayList<tPayment>> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(PaymentListActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(PaymentListActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                    paymentListAdapter.clear();
                }
                else{
                    paymentListAdapter.addAll(response.body());
                    payAllBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tPayment>> call, Throwable t) {

            }
        });
    }

    private void updatePaymentStatus(String paymentID){
        Call<Boolean> callUpdatePaymentStatus = Helper.getParkourService().tPaySingle(tUser.getInstance().getId(), paymentID);
        callUpdatePaymentStatus.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(PaymentListActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(PaymentListActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(PaymentListActivity.this, "Paid Successful", Toast.LENGTH_SHORT).show();
                    getPaymentList();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    private void updateAllPaymentStatus(){
        Call<Boolean> callUpdatePaymentStatus = Helper.getParkourService().tPayAll(tUser.getInstance().getId());
        callUpdatePaymentStatus.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(PaymentListActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(PaymentListActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(PaymentListActivity.this, "Paid Successful", Toast.LENGTH_SHORT).show();
                    getPaymentList();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.pay_all_button)
    public void payAll(){
        Intent intent = new Intent(this, StripeCardInputActivity.class);
        intent.putExtra("allFlag", true);
        startActivityForResult(intent, GlobalVar.REQUEST_CODE_STRIPE);
    }
}
