package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ErrorDialogFragment;
import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.tObject.tPayment;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;
import com.stripe.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;
import com.stripe.model.Charge;
import com.stripe.model.Order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StripeCardInputActivity extends AppCompatActivity {

    @BindView(R.id.card_input_widget) CardInputWidget cardInputWidget;
    @BindView(R.id.pay_button) Button payBtn;

    @BindView(R.id.date_enter_text_view) TextView dateEnterTv;
    @BindView(R.id.amount_text_view) TextView amountTv;
    @BindView(R.id.time_enter_text_view) TextView timeEnterTv;
    @BindView(R.id.time_out_text_view) TextView timeOutTv;

    @BindView(R.id.time_linear_layout) LinearLayout timeLl;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    Unbinder unbinder;

    String paymentAmount;
    String paymentID;
    String dateEnter;
    String timeEnter;
    String timeOut;

    boolean allFlag;

    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe_card_input);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle("Add Credit Card Information");
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        allFlag = intent.getBooleanExtra("allFlag", false);
        if(allFlag){
            getTotalPaymentAmount();
            paymentID = "-1";
        }
        else{
            paymentID = intent.getStringExtra("paymentID");
            paymentAmount = intent.getStringExtra("paymentAmount");
            dateEnter = intent.getStringExtra("parkingDate");
            timeEnter = intent.getStringExtra("timeEnter");
            timeOut = intent.getStringExtra("timeOut");

            amountTv.setText("Rp. " + paymentAmount + ",00");
            dateEnterTv.setText(dateEnter);
            timeEnterTv.setText(timeEnter);
            timeOutTv.setText(timeOut);

            dateEnterTv.setVisibility(View.VISIBLE);
            timeLl.setVisibility(View.VISIBLE);

            payBtn.setEnabled(true);
        }

//        amountTv.setText("Rp. " + paymentAmount + ",00");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.pay_button)
    public void test(){
        Card cardToSave = cardInputWidget.getCard();
        if(cardToSave == null || !cardToSave.validateCard()){
            Toast.makeText(this, "Invalid Card Data", Toast.LENGTH_SHORT).show();
        }
        else{
            payBtn.setEnabled(false);
            try{
                com.stripe.android.Stripe stripe = new com.stripe.android.Stripe(this, "pk_test_nnmu8oYhvw9Ky0BVhexpu79G");
                stripe.createToken(
                        cardToSave,
                        new TokenCallback() {
                            @Override
                            public void onError(Exception error) {
                                Toast.makeText(StripeCardInputActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onSuccess(Token token) {
//                                Stripe.apiKey = "sk_test_d0VHHAyIRwcw33Jwln1Wnr33";

//                                Toast.makeText(StripeCardInputActivity.this, token.getId(), Toast.LENGTH_LONG).show();

                                Map<String, Object> params = new HashMap<String, Object>();
                                params.put("amount", paymentAmount + "00");
                                params.put("currency", "idr");
                                params.put("description", "Example charge");
                                params.put("source", token.getId());

                                chargePayment(params);
                            }
                        }
                );
            }
            catch (Exception e){
                Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                payBtn.setEnabled(true);
            }
        }
    }

    private void chargePayment(final Map<String, Object> chargeParams){
        new AsyncTask<Void, Void, Void>() {

            Charge charge;

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    com.stripe.Stripe.apiKey = "sk_test_d0VHHAyIRwcw33Jwln1Wnr33";
                    charge = Charge.create(chargeParams);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(Void result) {
                Toast.makeText(StripeCardInputActivity.this, "Card Charged : " + charge.getCreated() + "\nPaid : " +charge.getPaid(), Toast.LENGTH_LONG).show();
                if(charge.getPaid().equals(true)){
                    Intent backIntent = new Intent();
                    backIntent.putExtra("paymentID", paymentID);
                    backIntent.putExtra("allFlag", allFlag);
                    setResult(RESULT_OK, backIntent);
                    finish();
                }
            }

        }.execute();
    }

    private void getTotalPaymentAmount(){
        Call<ArrayList<tPayment>> callGetPaymentList = Helper.getParkourService().tGetPendingPayment(tUser.getInstance().getId());
        callGetPaymentList.enqueue(new Callback<ArrayList<tPayment>>() {
            @Override
            public void onResponse(Call<ArrayList<tPayment>> call, Response<ArrayList<tPayment>> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(StripeCardInputActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(StripeCardInputActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    long tempPaymentAmount = 0;
                    for(int i = 0; i < response.body().size(); i++){
                        long currentPaymentAmount = Long.parseLong(response.body().get(i).getPayment_amount());
//                        Log.i("currentPaymentAmount", Long.toString(currentPaymentAmount));
                        tempPaymentAmount += currentPaymentAmount;
//                        Log.i("totalPaymentAmount", Long.toString(tempPaymentAmount));
                    }
                    paymentAmount = Long.toString(tempPaymentAmount);
                    amountTv.setText("Rp. " + paymentAmount + ",00");
//                    Log.i("totalPaymentAmount", paymentAmount);
                    payBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tPayment>> call, Throwable t) {

            }
        });
    }

}
