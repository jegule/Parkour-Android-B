package com.hkm.apps.parkour_beta.Object;

/**
 * Created by jegul on 15/02/2017.
 */

public class Time {

    private int hour;
    private int minute;

    public Time(){}

    public int getHour(){
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    @Override
    public String toString() {
        String hour = Integer.toString(getHour());
        String minute = Integer.toString(getMinute());

        if(hour.length() < 2){
            hour = '0' + hour;
        }
        if(minute.length() < 2){
            minute = '0' + minute;
        }

        return hour + "." + minute;
    }
}
