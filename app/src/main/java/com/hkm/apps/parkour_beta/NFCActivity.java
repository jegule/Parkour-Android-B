package com.hkm.apps.parkour_beta;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NFCActivity extends Activity {
    // NFC handling stuff
    PendingIntent pendingIntent;
    NfcAdapter nfcAdapter;

    @BindView(R.id.nfc_tag)
    TextView nfcTv;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

        unbinder = ButterKnife.bind(this);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        handleNFCAdapter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        //Toast.makeText(this, "NFC intent received.", Toast.LENGTH_LONG).show();
        super.onNewIntent(intent);

        if(intent.hasExtra(NfcAdapter.EXTRA_TAG)){
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String uid = Helper.bytesToHexString(tag.getId()).toUpperCase();
//            Toast.makeText(this, uid, Toast.LENGTH_LONG).show();
            Intent resultIntent = new Intent();
            resultIntent.putExtra(GlobalVar.NFC_TAG, uid);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        handleNFCAdapter();

        enableForegroundDispatch();

        super.onResume();
    }

    @Override
    protected void onPause() {
        disableForegroundDispatch();
        super.onPause();
    }

    private void enableForegroundDispatch(){
        Intent intent = new Intent(this, NFCActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        IntentFilter[] intentFilters = new IntentFilter[]{};

        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);
    }

    private void disableForegroundDispatch(){
        nfcAdapter.disableForegroundDispatch(this);
    }

    private void handleNFCAdapter(){
        if(nfcAdapter == null){
            Toast.makeText(this, "NFC is not supported.", Toast.LENGTH_LONG).show();
            finish();
        }
        else{
            if(nfcAdapter.isEnabled()){
                /*Toast.makeText(this, "NFC is enabled.", Toast.LENGTH_LONG).show();*/
                nfcTv.setText("Please scan your eKTP at the back of your smartphone.");
            }
            else{
                Toast.makeText(this, "NFC is disabled.", Toast.LENGTH_LONG).show();
                nfcTv.setText("Please enable NFC on your phone");
            }
        }
    }
}
