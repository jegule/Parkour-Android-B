package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Adapter.CarSpinnerAdapter;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.tObject.tCar;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarChooserActivity extends AppCompatActivity {

    @BindView(R.id.car_list_spinner) Spinner carSpinner;
//    @BindView(R.id.last_used_text_view) TextView lastUsedTv;

    @BindView(R.id.to_summary_button) Button nextBtn;

    @BindView(R.id.toolbar) Toolbar toolbar;

    //CarSpinnerAdapter spinnerAdapter;
    ArrayList<String> carPlateList;

    ArrayAdapter<String> spinnerAdapter;

    Unbinder unbinder;

    String currentCarID;
    String userID = "";

    final Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_chooser);

        unbinder = ButterKnife.bind(this);

        userID = tUser.getInstance().getId();

        toolbar.setTitle("Choose Car");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        execute();
    }

    private void execute(){
        carPlateList = new ArrayList<String>();

        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, carPlateList);
        carSpinner.setAdapter(spinnerAdapter);

        carSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentCarID = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Call<ArrayList<tCar>> callGetCar = Helper.getParkourService().tGetCar(userID);
        callGetCar.enqueue(new Callback<ArrayList<tCar>>() {
            @Override
            public void onResponse(Call<ArrayList<tCar>> call, Response<ArrayList<tCar>> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(CarChooserActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(CarChooserActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
//                    Toast.makeText(CarChooserActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                    for(int i = 0; i < response.body().size(); i++){
                        spinnerAdapter.add(response.body().get(i).getPlate_number());
                    }
                    spinnerAdapter.notifyDataSetChanged();

                    if(carSpinner.getCount() > 0){
                        nextBtn.setEnabled(true);
                    }
                    else{
                        nextBtn.setEnabled(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tCar>> call, Throwable t) {

            }
        });

        /*if(lastUsedList.isEmpty()){
            Toast.makeText(this, "Please add a car first on the main menu to continue", Toast.LENGTH_LONG).show();
        }
        else{
            nextBtn.setEnabled(true);
        }*/

        /*carSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(lastUsedList.get(position) != null){
                    lastUsedTv.setText(lastUsedList.get(position).toString());
                }
                else{
                    lastUsedTv.setText("Never");
                }

                currentCarID = carPlateList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(GlobalVar.INTENT_FAILED_CODE, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.to_summary_button)
    public void goToSummary(){
        Preferences.getPref().setString("carID", currentCarID);

        Intent intent = new Intent(this, BookingSummaryActivity.class);
        startActivityForResult(intent, GlobalVar.REQUEST_CODE_BOOKING_SUMMARY_ACTIVITY);
//        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GlobalVar.REQUEST_CODE_BOOKING_SUMMARY_ACTIVITY){
            if(resultCode == GlobalVar.INTENT_FAILED_CODE){
                execute();
            }
            else if(resultCode == GlobalVar.INTENT_SUCCESS_CODE){
                Intent intent = new Intent();
                setResult(GlobalVar.INTENT_SUCCESS_CODE, intent);
                finish();
            }
        }
    }
}
