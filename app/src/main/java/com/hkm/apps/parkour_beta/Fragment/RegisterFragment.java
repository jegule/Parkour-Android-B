package com.hkm.apps.parkour_beta.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.NFCActivity;
import com.hkm.apps.parkour_beta.Object.ParkourResponse;
import com.hkm.apps.parkour_beta.Object.UserResponseObject;
import com.hkm.apps.parkour_beta.R;
import com.hkm.apps.parkour_beta.tObject.tTemplate;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    @BindView(R.id.reg_email) EditText emailEt;
    @BindView(R.id.reg_fullname) EditText fnameEt;
    @BindView(R.id.reg_password) EditText passEt;
//    @BindView(R.id.ektp_id) EditText ektpEt;

    Unbinder unbinder;

    Gson gson = new Gson();

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.reg_btn)
    public void registerUser(){
        String email = emailEt.getText().toString();
        String fullname = fnameEt.getText().toString();
        String password = passEt.getText().toString();
//        String eKtpID = ektpEt.getText().toString();

        if(TextUtils.isEmpty(email) ||
                TextUtils.isEmpty(fullname) ||
                TextUtils.isEmpty(password)){
            Toast.makeText(getActivity(), "One or more fields are missing.", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getContext(), "Please wait...", Toast.LENGTH_SHORT).show();

            /*Call<ParkourResponse<UserResponseObject>> registerRespCall = Helper.getParkourService().userRegister(email, fullname, password, eKtpID);
            registerRespCall.enqueue(new Callback<ParkourResponse<UserResponseObject>>() {
                @Override
                public void onResponse(Call<ParkourResponse<UserResponseObject>> call, Response<ParkourResponse<UserResponseObject>> response) {
                    Toast.makeText(getActivity(), response.body().getRespMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<ParkourResponse<UserResponseObject>> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });*/

            Call<tTemplate> callUserReg = Helper.getParkourService().tUserRegister(fullname, email, password);
            callUserReg.enqueue(new Callback<tTemplate>() {
                @Override
                public void onResponse(Call<tTemplate> call, Response<tTemplate> response) {
                    if(response.code() == 404){
                        try{
                            Toast.makeText(getContext(), gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e){
                            Toast.makeText(getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<tTemplate> call, Throwable t) {

                }
            });

            /*Call<UserResponse> registerRespCall = Helper.getParkourService().userRegister(email, fullname, password, eKtpID);
            registerRespCall.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    Toast.makeText(getActivity(), response.body().getRespMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });*/
        }
    }

    /*@OnClick(R.id.nfc_btn)
    public void scanNFC(){
        Intent intent = new Intent(getContext(), NFCActivity.class);
        startActivityForResult(intent, GlobalVar.NFC_REQ_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GlobalVar.NFC_REQ_CODE){
            if(resultCode == Activity.RESULT_OK){
                String tagUID = data.getStringExtra(GlobalVar.NFC_TAG);
                Toast.makeText(getContext(), tagUID, Toast.LENGTH_LONG).show();
                ektpEt.setText(tagUID);
            }
        }
    }*/
}
