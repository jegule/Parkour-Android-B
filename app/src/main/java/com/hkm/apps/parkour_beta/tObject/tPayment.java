package com.hkm.apps.parkour_beta.tObject;

/**
 * Created by jegul on 12/03/2017.
 */

public class tPayment {

    private String payment_amount;
    private String time_enter;
    private String time_out;
    private String duration;

    private String id;
    private String user_id;
    private String reservation_status;
    private String status;

    private String date_enter;

    public tPayment(){}

    public String getPayment_amount() {
        return payment_amount;
    }

    public String getTime_enter() {
        return time_enter;
    }

    public String getTime_out() {
        return time_out;
    }

    public String getDuration() {
        return duration;
    }

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getReservation_status() {
        return reservation_status;
    }

    public String getStatus() {
        return status;
    }

    public String getDate_enter() {
        return date_enter;
    }
}
