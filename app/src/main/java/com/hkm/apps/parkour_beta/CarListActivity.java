package com.hkm.apps.parkour_beta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Adapter.CarListAdapter;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.Object.ParkourResponse;
import com.hkm.apps.parkour_beta.Object.Plate;
import com.hkm.apps.parkour_beta.Object.UserResponseObject;
import com.hkm.apps.parkour_beta.tObject.tCar;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.security.AccessController.getContext;

public class CarListActivity extends AppCompatActivity {

    @BindView(R.id.plate_add_text) EditText plateEt;
    @BindView(R.id.car_list_view) ListView carListView;

    @BindView(R.id.toolbar) Toolbar mToolbar;

    ArrayList<tCar> plateList;
    CarListAdapter carListAdapter;

    Unbinder unbinder;

    final Gson gson = new Gson();
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);

        unbinder = ButterKnife.bind(this);

        mToolbar.setTitle("Car List");
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userID = tUser.getInstance().getId();

        plateList = new ArrayList<tCar>();
        carListAdapter = new CarListAdapter(this, plateList);
        carListView.setAdapter(carListAdapter);

        getCarList();

        InputFilter spaceBarFilter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isWhitespace(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }

        };

        plateEt.setFilters(new InputFilter[]{
                new InputFilter.AllCaps(),
                spaceBarFilter
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.add_car_button)
    public void addCar(){
        String userID = tUser.getInstance().getId();
        final String carPlate = plateEt.getText().toString();

        if(TextUtils.isEmpty(carPlate)){
            Toast.makeText(this, "Plate Number is missing", Toast.LENGTH_SHORT).show();
        }
        else{
            /*Call<ParkourResponse<UserResponseObject>> addCarCall = Helper.getParkourService().addCarPlate(userID, carPlate);
            addCarCall.enqueue(new Callback<ParkourResponse<UserResponseObject>>() {
                @Override
                public void onResponse(Call<ParkourResponse<UserResponseObject>> call, Response<ParkourResponse<UserResponseObject>> response) {
                    if(response.body().getRespType().equals("success")){

                        Gson gson = new Gson();
                        String userInfo = gson.toJson(response.body().getRespObject());
                        Preferences.getPref().setString(UserResponseObject.class.getSimpleName(), userInfo);

                        ArrayList<Plate> tempPlate = new ArrayList<Plate>(Arrays.asList(UserResponseObject.getInstance().getPlateList()));

                        carListAdapter.addAll(tempPlate);

                        Toast.makeText(CarListActivity.this, response.body().getRespMessage(), Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(CarListActivity.this, response.body().getRespMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ParkourResponse<UserResponseObject>> call, Throwable t) {
                    Toast.makeText(CarListActivity.this, "An error has occured", Toast.LENGTH_LONG).show();
                }
            });*/

            Call<Boolean> callAddCar = Helper.getParkourService().tAddCar(userID, carPlate);
            callAddCar.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if(response.code() == 404){
                        try{
                            Toast.makeText(CarListActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e){
                            Toast.makeText(CarListActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        getCarList();
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {

                }
            });
        }
    }

    private void getCarList(){
        Call<ArrayList<tCar>> callGetCar = Helper.getParkourService().tGetCar(userID);
        callGetCar.enqueue(new Callback<ArrayList<tCar>>() {
            @Override
            public void onResponse(Call<ArrayList<tCar>> call, Response<ArrayList<tCar>> response) {
                if(response.code() == 404){
                    try{
                        Toast.makeText(CarListActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e){
                        Toast.makeText(CarListActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    carListAdapter.addAll(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<tCar>> call, Throwable t) {

            }
        });
    }
}
