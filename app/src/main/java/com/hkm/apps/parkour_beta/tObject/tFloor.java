package com.hkm.apps.parkour_beta.tObject;

/**
 * Created by jegul on 01/03/2017.
 */

public class tFloor {

    private String id;
    private String node_id;
    private String slot_id;
    private String building_id;
    private String floor;
    private String user_id;
    private String plate_number;
    private String status;
    private String book_status;
    private String error_code;

    public tFloor() {
    }

    public String getId() {
        return id;
    }

    public String getNode_id() {
        return node_id;
    }

    public String getSlot_id() {
        return slot_id;
    }

    public String getBuilding_id() {
        return building_id;
    }

    public String getFloor() {
        return floor;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getPlate_number() {
        return plate_number;
    }

    public String getStatus() {
        return status;
    }

    public String getBook_status() {
        return book_status;
    }

    public String getError_code() {
        return error_code;
    }
}
