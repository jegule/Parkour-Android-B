package com.hkm.apps.parkour_beta.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hkm.apps.parkour_beta.R;
import com.hkm.apps.parkour_beta.tObject.tCar;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by jegul on 03/03/2017.
 */

public class CarSpinnerAdapter extends ArrayAdapter<tCar> {

    ArrayList<tCar> mPlateList;

    public CarSpinnerAdapter(Context context, ArrayList<tCar> carList) {
        super(context, 0, carList);
        mPlateList = carList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_car, parent, false);
        }

        tCar currentCar = mPlateList.get(position);

        TextView plateIdTv = (TextView) convertView.findViewById(R.id.car_plate_view);

        plateIdTv.setText(currentCar.getPlate_number());

        /*if(currentCar.getLastUsed() == null){
            plateLastUsedTv.setText("Never");
        }
        else{
            plateLastUsedTv.setText(currentPlate.getLastUsed().toString());
        }*/

        return convertView;

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        tCar currentCar = mPlateList.get(position);

        TextView plateIdTv = (TextView) convertView.findViewById(android.R.id.text1);

        plateIdTv.setText(currentCar.getPlate_number());

        /*if(currentCar.getLastUsed() == null){
            plateLastUsedTv.setText("Never");
        }
        else{
            plateLastUsedTv.setText(currentPlate.getLastUsed().toString());
        }*/

        return convertView;
    }

    @Override
    public void addAll(Collection<? extends tCar> collection) {
        mPlateList.clear();
        mPlateList.addAll(collection);
        notifyDataSetChanged();
    }

    @Override
    public void add(@Nullable tCar object) {
        mPlateList.add(object);
        notifyDataSetChanged();
    }
}
