package com.hkm.apps.parkour_beta.Helper;

/**
 * Created by jegul on 11/02/2017.
 */

public class GlobalVar {

//    public static final String BASE_API_URI = "http://139.192.143.176:3000/";
//    public static final String BASE_API_URI = "http://192.168.0.102:3000/";
//    public static final String BASE_API_URI = "http://118.136.214.167:3000/";

    public static final String BASE_URI = "http://renhard.net/parkour";
//    public static final String BASE_URI = "http://192.168.0.194/parking/index.php/";
    public static final String BASE_URI_ = "/parking/index.php";
    public static final String BASE_API_URI = BASE_URI + "/Server/";
    public static final String BASE_API_URI_ = BASE_URI_ + "/Server/";
    public static final String BASE_FLOOR_IMAGE_URI = "http://192.168.0.194/parking/assets/blueprint/";
    public static final String BASE_FLOOR_IMAGE_URI_ = "/parking/assets/blueprint/";
//    public static final String BASE_FLOOR_IMAGE_URI_ = "/assets/blueprint/";

    public static int NFC_REQ_CODE = 101;
    public static int NFC_SUCCESS_CODE = 111;
    public static int NFC_FAILED_CODE = 112;
    public static int INTENT_SUCCESS_CODE = 113;
    public static int INTENT_FAILED_CODE = 114;

    public static int REQUEST_CODE_FINE_LOCATION_PERMISSION = 201;

    public static final String NFC_TAG = "NFC_TAG";

    public static final int REQUEST_CODE_PLACE_DETAIL_ACTIVITY = 301;
    public static final int REQUEST_CODE_SLOT_CHOOSER_ACTIVITY = 302;
    public static final int REQUEST_CODE_CAR_CHOOSER_ACTIVITY = 303;
    public static final int REQUEST_CODE_BOOKING_SUMMARY_ACTIVITY = 304;

    public static final int REQUEST_CODE_STRIPE = 965;

    public static final int REQUEST_CODE_DRAWER_ITEM = 777;
}