package com.hkm.apps.parkour_beta.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.MainActivity;
import com.hkm.apps.parkour_beta.R;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.hkm.apps.parkour_beta.tObject.tUser;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    @BindView(R.id.login_email) EditText emailEt;
    @BindView(R.id.login_password) EditText passEt;

    Unbinder unbinder;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        unbinder = ButterKnife.bind(this, view);

        String email = Preferences.getPref().getString("email", null);
        String password = Preferences.getPref().getString("password", null);

        if (!TextUtils.isEmpty(email)) {
            emailEt.setText(email);
            passEt.setText(password);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.login_btn)
    public void loginUser() {
        final String email = emailEt.getText().toString();
        final String password = passEt.getText().toString();

        if (TextUtils.isEmpty(email) ||
                TextUtils.isEmpty(password)) {
            Toast.makeText(getActivity(), "One or more fields are missing.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Please wait...", Toast.LENGTH_SHORT).show();

            /*Call<ParkourResponse<UserResponseObject>> loginRespCall = Helper.getParkourService().userLogin(email, password);
            loginRespCall.enqueue(new Callback<ParkourResponse<UserResponseObject>>() {
                @Override
                public void onResponse(Call<ParkourResponse<UserResponseObject>> call, Response<ParkourResponse<UserResponseObject>> response) {
                    if (response.body().getRespType().equals("success")) {
                        Preferences.getPref().setString("email", email);
                        Preferences.getPref().setString("password", password);

                        Gson gson = new Gson();
                        String userInfo = gson.toJson(response.body().getRespObject());
                        Preferences.getPref().setString(UserResponseObject.class.getSimpleName(), userInfo);
                        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Preferences.getPref().setString("email", null);
                        Preferences.getPref().setString("password", null);
                        Toast.makeText(getActivity(), response.body().getRespMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ParkourResponse<UserResponseObject>> call, Throwable t) {
                    if (email.equals("dhdh") && password.equals("zhsh")) {
                        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });*/
            final Gson gson = new Gson();

            Call<ArrayList<tUser>> callLogin = Helper.getParkourService().tUserLogin(email, password);
            callLogin.enqueue(new Callback<ArrayList<tUser>>() {
                @Override
                public void onResponse(Call<ArrayList<tUser>> call, Response<ArrayList<tUser>> response) {
                    if(response.code() == 404){
                        try{
                            Toast.makeText(getContext(), gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e){
                            Toast.makeText(getContext(), "An error has occured", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Preferences.getPref().setString("email", email);
                        Preferences.getPref().setString("password", password);


                        String tUserInfo = gson.toJson(response.body().get(0));
                        Preferences.getPref().setString(tUser.class.getSimpleName(), tUserInfo);
                        Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<tUser>> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
