package com.hkm.apps.parkour_beta.Object;

/**
 * Created by jegul on 15/02/2017.
 */

public class Location {

    private double latitude;
    private double longitude;

    public Location(){

    }

    public double getLatitude(){
        return latitude;
    }

    public double getLongitude(){
        return longitude;
    }
}
