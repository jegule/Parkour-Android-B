package com.hkm.apps.parkour_beta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hkm.apps.parkour_beta.Adapter.ButtonGridAdapter;
import com.hkm.apps.parkour_beta.Adapter.SpinnerAdapter;
import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.Helper.Helper;
import com.hkm.apps.parkour_beta.Helper.Preferences;
import com.hkm.apps.parkour_beta.Object.Floor;
import com.hkm.apps.parkour_beta.Object.ParkingPlaceResponseObject;
import com.hkm.apps.parkour_beta.Object.ParkourResponse;
import com.hkm.apps.parkour_beta.tObject.tFloor;
import com.hkm.apps.parkour_beta.tObject.tTemplate;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.senab.photoview.PhotoView;

public class SlotChooserActivity extends AppCompatActivity {

    @BindView(R.id.floor_list_spinner) Spinner floorSpinner;
//    @BindView(R.id.floor_plan_image) PhotoView floorPlanImageView;
//    @BindView(R.id.available_slot_list_spinner) Spinner slotSpinner;

//    @BindView(R.id.choose_car_button) Button chooseCarBtn;

    @BindView(R.id.btn_grid) GridView gridView;

    @BindView(R.id.toolbar) Toolbar toolbar;

    Unbinder unbinder;
    String placeID;

    SpinnerAdapter spinnerAdapter;

    ArrayList<String> arrayList;
    ArrayList<String> slotList;
    ArrayList<String> floorImageUrlList;

    ArrayList<tFloor> floorArrayList;

    Map<String, ArrayList<tFloor>> slotMap;
    SpinnerAdapter slotListAdapter;

    String tempFloorNumber;
    String currentFloorNumber;

    int counter = 0;

    boolean firstTimeFlag = true;

    int floorCount;

    ButtonGridAdapter buttonGridList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_slot_chooser);
        setContentView(R.layout.activity_slot_chooser_withbtn);

        unbinder = ButterKnife.bind(this);

        /*Intent intent = getIntent();
        placeID = intent.getStringExtra("id");*/

        toolbar.setTitle("Choose Slot");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        execute();
    }

    private void execute(){
        placeID = Preferences.getPref().getString("placeID", null);
        floorCount = Integer.parseInt(Preferences.getPref().getString("floorCount", "0"));

        arrayList = new ArrayList<String>();
        spinnerAdapter = new SpinnerAdapter(this, android.R.layout.simple_list_item_1, arrayList);
        floorSpinner.setAdapter(spinnerAdapter);

        slotList = new ArrayList<String>();
        floorArrayList = new ArrayList<tFloor>();

        slotMap = new HashMap<String, ArrayList<tFloor>>();
        slotListAdapter = new SpinnerAdapter(this, android.R.layout.simple_list_item_1, slotList);
//        slotSpinner.setAdapter(slotListAdapter);

        buttonGridList = new ButtonGridAdapter(this, floorArrayList);
        gridView.setAdapter(buttonGridList);

        floorImageUrlList = new ArrayList<String>();

        floorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentFloorNumber = parent.getItemAtPosition(position).toString();
//                slotListAdapter.addAll(slotMap.get(currentFloorNumber));
//                floorArrayList.clear();
//                floorArrayList.addAll(slotMap.get(currentFloorNumber));
                buttonGridList.setCurrentFloorNumber(Integer.parseInt(currentFloorNumber));
                buttonGridList.addAll(slotMap.get(currentFloorNumber));

                gridView.invalidateViews();


//                buttonGridList.notifyDataSetChanged();

                /*if(slotSpinner.getCount() == 0){
                    chooseCarBtn.setEnabled(false);
                }
                else{
                    chooseCarBtn.setEnabled(true);
                }*/

                /*if(!firstTimeFlag){
                    try{
//                        Picasso.with(SlotChooserActivity.this).load(Preferences.getPref().getString("ip", "http://192.168.0.195") + GlobalVar.BASE_FLOOR_IMAGE_URI_ + placeID + "_" + currentFloorNumber + ".jpg").into(floorPlanImageView);
//                        Picasso.with(SlotChooserActivity.this).load(GlobalVar.BASE_URI + GlobalVar.BASE_FLOOR_IMAGE_URI_ + placeID + "_" + currentFloorNumber + ".jpg").into(floorPlanImageView);
                    }
                    catch (Exception e){
                        Log.i("image", e.getMessage());
                        Toast.makeText(SlotChooserActivity.this, "Error loading image.", Toast.LENGTH_SHORT).show();
                    }
                }*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//                chooseCarBtn.setEnabled(false);
                /*floorSpinner.setSelection(0);
                floorArrayList.clear();
                floorArrayList.addAll(slotMap.get(1));

                buttonGridList.setCurrentFloorNumber(1);
                buttonGridList.notifyDataSetChanged();*/

                floorSpinner.setSelection(-1);
                gridView.invalidateViews();
            }
        });

        final Gson gson = new Gson();

        Log.i("floor", Integer.toString(floorCount));

        if(floorCount != 0){
            for(int i = 1; i <= floorCount; i++){
                final String floorNum = Integer.toString(i);
                Call<ArrayList<tFloor>> callGetFloor = Helper.getParkourService().tGetFloor(placeID, floorNum);
                callGetFloor.enqueue(new Callback<ArrayList<tFloor>>() {
                    @Override
                    public void onResponse(Call<ArrayList<tFloor>> call, Response<ArrayList<tFloor>> response) {
                        spinnerAdapter.add(floorNum);
                        if(response.code() == 404){
                            try{
//                                Toast.makeText(SlotChooserActivity.this, gson.fromJson(response.errorBody().string(), tTemplate.class).getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e){
//                                Toast.makeText(SlotChooserActivity.this, "An error has occured", Toast.LENGTH_SHORT).show();
                            }
                            slotMap.put(floorNum, new ArrayList<tFloor>());
                        }
                        else{
                            /*ArrayList<String> tempSlotList = new ArrayList<String>();
                            for(int j = 0; j < response.body().size(); j++){
                                tempSlotList.add(response.body().get(j).getSlot_id());
                            }*/

                            ArrayList<tFloor> tempSlotList = new ArrayList<tFloor>();

                            if(response.body().size() != 0){
                                tempSlotList.addAll(response.body());
                            }

//                            floorArrayList.addAll(response.body());

                            slotMap.put(floorNum, tempSlotList);
                            Log.i("slotMap", slotMap.toString());
                        }

                        counter++;

                        if(counter >= floorCount){
                            buttonGridList.setCurrentFloorNumber(1);
//                            gridView.notify();
//                        slotListAdapter.addAll(slotMap.get(currentFloorNumber));
//                        floorArrayList.addAll(slotMap.get(currentFloorNumber));

                            spinnerAdapter.notifyDataSetChanged();
                            floorSpinner.setSelection(-1);

                            gridView.invalidateViews();
//                            spinnerAdapter.notifyDataSetChanged();
//                            slotListAdapter.notifyDataSetChanged();
//
//                            buttonGridList.addAll(slotMap.get(1));
                        }
//                        currentFloorNumber = "1";
                        /*buttonGridList.setCurrentFloorNumber(1);
//                        slotListAdapter.addAll(slotMap.get(currentFloorNumber));
//                        floorArrayList.addAll(slotMap.get(currentFloorNumber));

//                        floorSpinner.setSelection(0);
                        spinnerAdapter.notifyDataSetChanged();
                        slotListAdapter.notifyDataSetChanged();

                        buttonGridList.notifyDataSetChanged();*/

                        /*try{
//                            Picasso.with(SlotChooserActivity.this).load(Preferences.getPref().getString("ip", "http://192.168.0.195") + GlobalVar.BASE_FLOOR_IMAGE_URI_ + placeID + "_" + currentFloorNumber + ".jpg").into(floorPlanImageView);
//                            Picasso.with(SlotChooserActivity.this).load(GlobalVar.BASE_URI + GlobalVar.BASE_FLOOR_IMAGE_URI_ + placeID + "_" + currentFloorNumber + ".jpg").into(floorPlanImageView);
                        }
                        catch (Exception e){
                            Log.i("image", e.getMessage());
                            Toast.makeText(SlotChooserActivity.this, "Error loading image.", Toast.LENGTH_SHORT).show();
                        }
                        finally {
                            firstTimeFlag = false;
                        }*/

                        /*if(slotSpinner.getCount() > 0){
                            chooseCarBtn.setEnabled(true);
                        }*/
                    }

                    @Override
                    public void onFailure(Call<ArrayList<tFloor>> call, Throwable t) {

                    }
                });
            }
        }

        /*Call<ParkourResponse<ParkingPlaceResponseObject>> getPlaceDetailCall = Helper.getParkourService().getPlaceDetail(placeID);
        getPlaceDetailCall.enqueue(new Callback<ParkourResponse<ParkingPlaceResponseObject>>() {
            @Override
            public void onResponse(Call<ParkourResponse<ParkingPlaceResponseObject>> call, Response<ParkourResponse<ParkingPlaceResponseObject>> response) {
                ParkingPlaceResponseObject placeDetail = response.body().getRespObject();
                Floor[] floorArray = placeDetail.getFloor();
                for(int i = 0; i < floorArray.length; i++){
                    tempFloorNumber = floorArray[i].getNumber() + "";
                    spinnerAdapter.add(tempFloorNumber);

                    ArrayList<String> tempSlotList = new ArrayList<String>();
                    for(int j = 0; j < floorArray[i].getSlot().length; j++){
                        tempSlotList.add(floorArray[i].getSlot()[j].getId() + "");
                    }
                    slotMap.put(tempFloorNumber, tempSlotList);

                    floorImageUrlList.add(floorArray[i].getPicture());
                }

            }

            @Override
            public void onFailure(Call<ParkourResponse<ParkingPlaceResponseObject>> call, Throwable t) {

            }
        });*/



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    /*@OnClick(R.id.choose_car_button)
    public void goToCarChooser(){
        Intent intent = new Intent(this, CarChooserActivity.class);
        *//*intent.putExtra("floorID", currentFloorNumber);
        intent.putExtra("slotID", slotSpinner.getSelectedItem().toString());*//*
        Preferences.getPref().setString("floorID", currentFloorNumber);
//        Preferences.getPref().setString("slotID", slotSpinner.getSelectedItem().toString());
        startActivityForResult(intent, GlobalVar.REQUEST_CODE_CAR_CHOOSER_ACTIVITY);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GlobalVar.REQUEST_CODE_CAR_CHOOSER_ACTIVITY){
            if(resultCode == GlobalVar.INTENT_FAILED_CODE){
                execute();
            }
            else if(resultCode == GlobalVar.INTENT_SUCCESS_CODE){
                Intent intent = new Intent();
                setResult(GlobalVar.INTENT_SUCCESS_CODE, intent);
                finish();
            }
        }
    }
}
