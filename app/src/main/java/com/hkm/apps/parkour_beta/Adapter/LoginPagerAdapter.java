package com.hkm.apps.parkour_beta.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.hkm.apps.parkour_beta.Fragment.LoginFragment;
import com.hkm.apps.parkour_beta.Fragment.RegisterFragment;
import com.hkm.apps.parkour_beta.R;

/**
 * Created by jegul on 11/02/2017.
 */

public class LoginPagerAdapter extends FragmentPagerAdapter {

    Context mContext;

    public LoginPagerAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        mContext = ctx;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return new LoginFragment();
        }
        else if(position == 1){
            return new RegisterFragment();
        }
        else{
            return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return mContext.getResources().getString(R.string.login);
        }
        else if(position == 1){
            return mContext.getResources().getString(R.string.register);
        }
        else{
            return null;
        }
    }
}
