package com.hkm.apps.parkour_beta.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hkm.apps.parkour_beta.Helper.GlobalVar;
import com.hkm.apps.parkour_beta.R;
import com.hkm.apps.parkour_beta.StripeCardInputActivity;
import com.hkm.apps.parkour_beta.tObject.tPayment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by jegul on 13/03/2017.
 */

public class PaymentListAdapter extends ArrayAdapter<tPayment> {

    ArrayList<tPayment> mPaymentList;

    public PaymentListAdapter(@NonNull Context context, ArrayList<tPayment> paymentList) {
        super(context, 0, paymentList);
        mPaymentList = paymentList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_payment, parent, false);
        }

        tPayment currentPayment = mPaymentList.get(position);

        TextView dateEnterTv = (TextView) convertView.findViewById(R.id.date_enter);
        dateEnterTv.setText(currentPayment.getDate_enter());

        TextView startTimeTv = (TextView) convertView.findViewById(R.id.time_enter);
        startTimeTv.setText(currentPayment.getTime_enter());

        TextView outTimeTv = (TextView) convertView.findViewById(R.id.time_out);
        outTimeTv.setText(currentPayment.getTime_out());

        TextView priceAmountTv = (TextView) convertView.findViewById(R.id.price_amount);
        priceAmountTv.setText("Rp. " + currentPayment.getPayment_amount() + ",00");

        Button payBtn = (Button) convertView.findViewById(R.id.pay_button);
        payBtn.setOnClickListener(onClickListener);

//        payBtn.setTag(currentPayment.getPayment_amount());
        payBtn.setTag(R.integer.payment_id, currentPayment.getId());
        payBtn.setTag(R.integer.payment_amount, currentPayment.getPayment_amount());
        payBtn.setTag(R.integer.parking_date, currentPayment.getDate_enter());
        payBtn.setTag(R.integer.time_enter, currentPayment.getTime_enter());
        payBtn.setTag(R.integer.time_out, currentPayment.getTime_out());

        return convertView;
    }

    @Override
    public void addAll(@NonNull Collection<? extends tPayment> collection) {
//        super.addAll(collection);
        mPaymentList.clear();
        mPaymentList.addAll(collection);
        notifyDataSetChanged();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /*Button b = (Button) v;
            b.setEnabled(false);*/
            String paymentID = (String) v.getTag(R.integer.payment_id);
            String paymentAmount = (String) v.getTag(R.integer.payment_amount);
            String parkingDate = (String) v.getTag(R.integer.parking_date);
            String timeEnter = (String) v.getTag(R.integer.time_enter);
            String timeOut = (String) v.getTag(R.integer.time_out);
//            Toast.makeText(getContext(), id, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getContext(), StripeCardInputActivity.class);
            intent.putExtra("allFlag", false);
            intent.putExtra("parkingDate", parkingDate);
            intent.putExtra("timeEnter", timeEnter);
            intent.putExtra("timeOut", timeOut);
            intent.putExtra("paymentID", paymentID);
            intent.putExtra("paymentAmount", paymentAmount);
            ((AppCompatActivity)getContext()).startActivityForResult(intent, GlobalVar.REQUEST_CODE_STRIPE);
        }
    };

    @Override
    public void clear() {
//        super.clear();
        mPaymentList.clear();
        notifyDataSetChanged();
    }
}
