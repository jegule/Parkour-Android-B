package com.hkm.apps.parkour_beta;

import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.hkm.apps.parkour_beta.Helper.Preferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class IpActivity extends AppCompatActivity {

    Unbinder unbinder;

    @BindView(R.id.ip)
    EditText ipEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ip);

        unbinder = ButterKnife.bind(this);

        ipEt.setText(Preferences.getPref().getString("ip", "http://192.168.0.195"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.set_ip_btn)
    public void setIp(){
        String ip = ipEt.getText().toString();
        Preferences.getPref().setString("ip", ip);
    }
}
